#include "functions.hpp"

#include <iostream>
#include <fstream>
#include <limits>
using namespace std;

/**
Displays the main menu of the program as well as a count of total movies.
@param savedMovies  The total amount of movies that have been stored.
*/
void DisplayMainMenu( int savedMovies )
{
    cout << endl << "---------------------------------" << endl;
    cout << "MOVIE COLLECTION" << endl
         << savedMovies << " total movie(s)" << endl << endl;
    cout << "MAIN MENU" << endl
         << "---------" << endl
         << "1. Add movie" << endl
         << "2. Update movie" << endl
         << "3. Clear all movies" << endl
         << "4. View all movies" << endl
         << "5. Save and quit" << endl;
}

/**
Gets integer input from the user and ensures that the input is between
a certain valid range before returning it.
@param min  The minimum number allowed.
@param max  The maximum number allowed.
@return The user's input, between min and max (inclusive).
*/
int GetChoice( int min, int max )
{
    int choice;
    cout << "(" << min << " - " << max << ") >> ";
    cin >> choice;

    while ( choice < min || choice > max )
    {
        cout << "Invalid choice, try again." << endl;
        cout << ">> ";
        cin >> choice;
    }

    return choice;
}

/**
Special code to clear the screen, works with Windows, Linux, Mac, and Unix.
*/
void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "cls" );
    #else
        system( "clear" );
    #endif
}

/**
Special code to pause the program and ask the user to hit ENTER to continue.
*/
void Pause()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        system( "pause" );
    #else
        cout << endl << " Press ENTER to continue..." << endl;
        cin.ignore( std::numeric_limits <std::streamsize> ::max(), '\n' );
    #endif
}

/**
Checks if the list of movies is full. If not,
adds a new movie to the end of the array and adds 1 to savedMovies.
@param movieList[]  An array of movie titles.
@param savedMovies  A reference to the total amount of movies stored.
@param MAX_MOVIES   The total amount of movies allowed.
*/
void AddMovie( string movieList[], int & savedMovies, const int MAX_MOVIES )
{
    if ( savedMovies == MAX_MOVIES )
    {
        cout << "Cannot add any new movies! List is full!" << endl;
        return;
    }

    cout << "New movie title: ";
    cin.ignore();
    getline( cin, movieList[ savedMovies ] );
    savedMovies++;

    cout << "Movie added." << endl;
}

/**
Allows the user to edit the title of an existing movie.
User needs to enter the index of the movie they wish to edit.
@param movieList[]  An array of movie titles.
@param savedMovies  The total amount of movies stored.
*/
void UpdateMovie( string movieList[], int savedMovies )
{
    ViewAllMovies( movieList, savedMovies );

    cout << endl << "Update which movie?" << endl;
    int choice = GetChoice( 0, savedMovies-1 );

    cout << endl;
    cout << "Update \"" << movieList[choice] << "\"" << endl;
    cout << "New title: ";
    cin.ignore();
    getline( cin, movieList[choice] );

    cout << endl << "Movie updated." << endl;
}

/**
Clears all the movies in the list and sets the savedMovies count to 0.
@param movieList[]  An array of movie titles.
@param savedMovies  A reference to the total amount of movies stored.
@param MAX_MOVIES   The total amount of movies allowed.
*/
void ClearAllMovies( string movieList[], int & savedMovies, const int MAX_MOVIES )
{
    savedMovies = 0;
    for ( int i = 0; i < MAX_MOVIES; i++ )
    {
        movieList[i] = "";
    }

    cout << "All movies cleared." << endl;
}

/**
Displays a list of all movies and their index numbers.
@param movieList[]  An array of movie titles.
@param savedMovies  The total amount of movies stored.
*/
void ViewAllMovies( string movieList[], int savedMovies )
{
    for ( int i = 0; i < savedMovies; i++ )
    {
        cout << i << ". " << movieList[i] << endl;
    }
}

/**
Saves all the movies to an output file.
@param movieList[]  An array of movie titles.
@param savedMovies  The total amount of movies stored.
*/
void SaveMovies( string movieList[], int savedMovies )
{
    ofstream output( "movies.txt" );
    for ( int i = 0; i < savedMovies; i++ )
    {
        output << movieList[i] << endl;
    }
    output.close();
}

/**
Loads all the movies from an output file.
@param movieList[]  An array of movie titles.
@param savedMovies  The total amount of movies stored.
*/
void LoadMovies( string movieList[], int & savedMovies )
{
    ifstream input( "movies.txt" );
    string movieTitle;

    while ( getline( input, movieTitle ) )
    {
        movieList[ savedMovies ] = movieTitle;
        savedMovies++;
    }

    input.close();
}
