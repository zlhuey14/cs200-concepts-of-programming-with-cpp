\contentsline {chapter}{\numberline {1}Setup}{2}
\contentsline {section}{\numberline {1.1}Turn-in instructions}{2}
\contentsline {section}{\numberline {1.2}Setup}{2}
\contentsline {chapter}{\numberline {2}For loop lab 1: Count Up}{4}
\contentsline {paragraph}{Program overview:}{4}
\contentsline {section}{\numberline {2.1}Reference information}{4}
\contentsline {subsection}{\numberline {2.1.1}For loops}{4}
\contentsline {section}{\numberline {2.2}Specifications}{5}
\contentsline {paragraph}{Program flow:}{5}
\contentsline {chapter}{\numberline {3}For loop lab 2: Multiply up}{6}
\contentsline {paragraph}{Program overview:}{6}
\contentsline {section}{\numberline {3.1}Specifications}{6}
\contentsline {paragraph}{Program information:}{6}
\contentsline {chapter}{\numberline {4}For loop lab 3: Summation}{7}
\contentsline {paragraph}{Program overview:}{7}
\contentsline {section}{\numberline {4.1}Specifications}{7}
\contentsline {paragraph}{Program flow:}{7}
\contentsline {chapter}{\numberline {5}For loop lab 4: Strings and Characters}{8}
\contentsline {paragraph}{Program overview:}{8}
\contentsline {section}{\numberline {5.1}Reference information}{9}
\contentsline {subsection}{\numberline {5.1.1}Comparing to 'A' vs. 'a'}{9}
\contentsline {subsection}{\numberline {5.1.2}getline( cin, var ) vs. cin $>>$ var}{9}
\contentsline {paragraph}{Issue: Skipping inputs}{9}
\contentsline {section}{\numberline {5.2}Specifications}{10}
\contentsline {paragraph}{Variables:}{10}
\contentsline {paragraph}{Program flow:}{10}
\contentsline {section}{\numberline {5.3}Testing:}{11}
