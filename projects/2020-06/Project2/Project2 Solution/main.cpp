#include <iostream>
#include <string>
using namespace std;

#include "functions.hpp"

int main()
{
    const int MAX_MOVIES = 10;
    string movieList[MAX_MOVIES];
    int savedMovies = 0;

    LoadMovies( movieList, savedMovies );

    bool done = false;
    while ( !done )
    {
        DisplayMainMenu( savedMovies );
        int choice = GetChoice( 1, 5 );
        cout << endl;

        switch( choice )
        {
            case 1: // add movie
            AddMovie( movieList, savedMovies, MAX_MOVIES );
            break;

            case 2: // update movie
            UpdateMovie( movieList, savedMovies );
            break;

            case 3: // clear all movies
            cin.ignore();
            ClearAllMovies( movieList, savedMovies, MAX_MOVIES );
            break;

            case 4: // view all movies
            cin.ignore();
            ViewAllMovies( movieList, savedMovies );
            break;

            case 5: // quit
            done = true;
            break;
        }

        Pause();
        ClearScreen();
    }

    SaveMovies( movieList, savedMovies );

    return 0;
}
