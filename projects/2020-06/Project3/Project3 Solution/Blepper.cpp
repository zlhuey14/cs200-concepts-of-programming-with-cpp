#include "Blepper.hpp"

BlepperApp::BlepperApp()
{
    m_done = false;
    m_state = SIGNED_OUT;
}

string BlepperApp::InputUsername()
{
    string username;
    cout << "Username (no spaces): ";
    cin >> username;
    return username;
}

string BlepperApp::InputPassword()
{
    string password;
    cout << "Password (no spaces): ";
    cin >> password;
    return password;
}

void BlepperApp::Run()
{
    while ( !m_done )
    {
        ProgramState nextState;

        switch ( m_state )
        {
            case SIGNED_OUT:        nextState = Menu_SignIn();          break;
            case CREATE_ACCOUNT:    nextState = Menu_CreateAccount();   break;
            case LOG_IN:            nextState = Menu_Login();           break;
            case DASHBOARD:         nextState = Menu_Dashboard();       break;
            case NEW_POST:          nextState = Menu_NewPost();       break;
            case VIEW_POSTS:        nextState = Menu_ViewPosts();       break;
            case QUIT:              m_done = true;                      break;
        }
        m_state = nextState;
    }
}

ProgramState BlepperApp::Menu_SignIn()
{
    Menu::Header( "BLEPPER" );
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Create account",
        "Log in",
        "Quit"
    } );

    switch( choice )
    {
        case 1: return CREATE_ACCOUNT; break;
        case 2: return LOG_IN; break;
        default: return QUIT;
    }
}

ProgramState BlepperApp::Menu_CreateAccount()
{
    Menu::Header( "Create Account" );

    string username = InputUsername();

    while ( m_accountManager.UsernameExists( username ) )
    {
        cout << endl << "** That username is already taken!" << endl
            << "Please choose another one!" << endl << endl;

        username = InputUsername();
    }

    string password = InputPassword();

    m_accountManager.AddAccount( username, password );

    cout << "New account created!" << endl;

    return SIGNED_OUT;
}

ProgramState BlepperApp::Menu_Login()
{
    Menu::Header( "Log in" );

    string username = InputUsername();
    string password = InputPassword();

    if ( !m_accountManager.UsernameExists( username ) )
    {
        cout << "User \"" << username << "\" does not exist!" << endl;
        return SIGNED_OUT;
    }

    if ( !m_accountManager.SignIn( username, password ) )
    {
        cout << "Incorrect password!" << endl;
        return SIGNED_OUT;
    }

    return DASHBOARD;
}

ProgramState BlepperApp::Menu_Dashboard()
{
    Menu::Header( "Blepper Dashboard - " + m_accountManager.GetLoggedInUsername() );
    int choice = Menu::ShowIntMenuWithPrompt( {
        "Post new blep",
        "View all bleps",
        "Sign out",
        "Quit"
    } );


    switch( choice )
    {
        case 1: return NEW_POST;        break;
        case 2: return VIEW_POSTS;      break;
        case 3: return SIGNED_OUT;      break;
        default: return QUIT;
    }
}

ProgramState BlepperApp::Menu_NewPost()
{
    Menu::Header( "Post new blep" );
    m_postManager.CreateNewPost( m_accountManager.GetLoggedInUsername() );

    return DASHBOARD;
}

ProgramState BlepperApp::Menu_ViewPosts()
{
    Menu::Header( "View all bleps" );
    m_postManager.PrintAllPosts();
    return DASHBOARD;
}
