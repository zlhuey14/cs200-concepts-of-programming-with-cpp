\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {Testing and Debugging}
\newcommand{\laTitle}       {}
\newcounter{question}

\renewcommand{\chaptername}{Topic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ }

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}

\chapter*{Tech Literacy Lab: ~\\Testing and Debugging}

\section*{Lab instructions}

	Once done with this lab, turn in \texttt{unittests.cpp} (testing) and a text document
	or scanned paper with debug values (debugging).

\hrulefill
\section*{Testing}

	~\\ Part 1: Download the \texttt{unittests.cpp} code, complete the functions, and turn that in as part of this assignment.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        
        Companies that write big software products will often have
        multiple types of testing that they do to ensure the validity
        of their software. ~\\
        
        \textbf{QA} (Quality Assurance) people generally will be responsible
        for writing \textbf{test cases} and running through \textbf{manual tests}
        to validate the software works as intended. They might also
        write \textbf{test scripts} that automate going through the UI of
        the software, or automate other parts of testing.
        
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
           \includegraphics[width=8cm]{images/hopeitworks.png}
    \end{subfigure}
\end{figure}

        \textbf{Software Engineers} will regularly need to be able to write
        \textbf{unit tests} along with whatever features they're working on
        in order to validate that their new feature works as intended \textit{and}
        they will run older unit tests to ensure that their new feature
        \textbf{doesn't break anything already there}. ~\\
        
        Testing is an important part of software development and,
        from a student perspective, it is also a handy tool in ensuring that
        your programming projects work as intended before turning them in.

\subsection*{Automating test cases}

	As covered before, we can write test cases to check our code by
	identifying a \textbf{set of inputs}, a \textbf{set of \underline{expected} outputs},
	and then running the program and comparing the \textbf{set of \underline{actual} outputs}.

	\begin{center}
		\includegraphics[width=12cm]{images/inputoutput.png}
	\end{center}
	
	This can be a handy roadmap to follow when running through your program's
	various features and validating it all works manually, but you can
	speed up the process by letting the computer do these checks for you.
	
	After all, when you manually validate outputs yourself, you're basically asking
	yourself, 
	
	\begin{center}
	``If the actual output DOES NOT match the expected output, ~\\
	then the test failed.''
	\end{center}
	
	\paragraph{Example: Area function} ~\\
	Let's say we have a function with the header 
	
	\begin{center}
	\texttt{int Area( int width, int length )}
	\end{center}
	
	and we don't necessarily know what's in it (we don't need that to test).
	We can write a series of test cases with inputs we specify and outputs
	we know to be correct in order to check the logic of the \texttt{Area} function...
	
	\begin{center}
		\begin{tabular}{c p{3cm} p{4cm} p{3cm}}
			\textbf{Test case} & \textbf{Inputs} & \textbf{Expected output} & \textbf{Actual output} \\ \hline
			1 & width = 10
			
				length = 20		& 120			\\ \hline
			2 & width = 2
			
				length = 5		& 10			\\ \hline
		\end{tabular}
	\end{center}
	
	...and so on.
	
	\newpage
	We could use this to manually test by calling \texttt{Area} and checking the output it gives you,
	but you could also write a function to validate it for us...

\begin{lstlisting}[style=code]
void Test_Area()
{
	// Variables used for each test
    int width, length;
    int expectedOutput;
    int actualOutput;

    // Test 1
    width = 10;
    length = 20;
    expectedOutput = 200;
    actualOutput = Area( width, length );

    if ( actualOutput != expectedOutput )
    {
        cout << "Test failed!"
            << "\n width: " << width
            << "\n length: " << length
            << "\n expected output: " << expectedOutput
            << "\n actual output: " << actualOutput 
            << endl;
    }
    else
    {
        cout << "Test 1 passed" << endl;
    }
}
\end{lstlisting}

	You can add as many tests as you'd like inside one function, testing
	different permutations of inputs against outputs to make sure that
	any reasonable scenarios are covered.
	To test, just call the function in your program, and check the output:
	
\begin{lstlisting}[style=output]
Test failed!
 width: 10
 length: 20
 expected output: 200
 actual output: 30
\end{lstlisting}

	If the test fails, you can \texttt{cout} your variables' values
	to help you figure out what might be wrong with the logic.
	
	\newpage
	A test that only validates \textbf{one function} is known as a 
	\textbf{unit test}, testing the smallest possible unit of a program.
	You could also write automated tests that checks several functions,
	but unit tests are good to validate each function on its own, independently of the rest.
	
	\paragraph{Assignment:} In the starter code you downloaded, there are several functions
	as well as a set of tests for those functions.
	
	When you first run the program, you'll see the tests already running and failing:
	
\begin{lstlisting}[style=output]
Test_GetPerimeter:

Test 1... FAIL
 width: 7, length: 3
 expected output: 20
 actual output:   0
\end{lstlisting}
	
	(There are more tests than this, but that's what it looks like.) ~\\
	
	You will implement the function definitions for the three functions:
	
	\begin{itemize}
		\item	\texttt{int GetPerimeter( int width, int height )}
		\item	\texttt{char GetFirstLetter( string text )}
		\item	\texttt{float Withdraw( float balance, float amount )}
	\end{itemize}
	
	and run the program to validate your work through those unit tests.
	These functions currently have \textbf{placeholder} code, so erase the placeholders
	and implement the logic as instructed by the comments above each one.
	
	\newpage
	\paragraph{The tests}
	The tests all follow a similar pattern:
	
	\begin{itemize}
		\item	Variables declared for whatever amount of inputs there are.
		\item	Variables declared for \texttt{expectedOutput} and \texttt{actualOutput}.
		\item	At the start of each test, the inputs and \texttt{expectedOutput} are assigned values.
		\item	The \texttt{actualOutput} variable receives its value from the function call.
		\item	An if/else statement is used to determine whether the test passed or failed.
	\end{itemize}
	
	~\\ A test for the Withdraw function:
\begin{lstlisting}[style=code]
// Test 1
balance = 100;
amount = 100;
expectedOutput = 0;
actualOutput = Withdraw( balance, amount );

cout << endl << "Test 1... ";
if ( actualOutput == expectedOutput )
{
    cout << "PASS" << endl;
}
else
{
    cout << "FAIL" << endl
        << " balance: " << balance 
        << ", amount: " << amount << endl
        << " expected output: " << expectedOutput 
        << endl
        << " actual output:   " << actualOutput 
        << endl;
}
\end{lstlisting}
	
	
	

\newpage
\section*{Debugging}

	~\\ Part 2: Download the \texttt{debugme.cpp} code and create a project
	with this code. We will be using it to look at debugging features common to IDEs.
	
	~\\
	First, build and run the program to make sure it runs on your system,
	and try to play the game for a little bit. The game is a little
	dungeon game, where you (The \texttt{'@'}) are trying to collect the
	\texttt{\$}. To move, you type a letter for a direction, then hit enter.

	\begin{center}
		\includegraphics[width=12cm]{images/debug-game.png}
	\end{center}
	
	\newpage
	\subsection*{Task 1: MovePlayer function: Variable values}
	
	Set a breakpoint on line 245, the first line in the MovePlayer function.

\begin{lstlisting}[style=code]
void MovePlayer( Coords& player, const Board& board, string direction )
{
    char firstLetter = direction[0];
    firstLetter = tolower( firstLetter );
    // ...
\end{lstlisting}

	\begin{hint}{How to set a breakpoint?}
		In most IDEs there's a little region to the left of the code you can
		click on to set a breakpoint. ~\\ ~\\
		\begin{tabular}{c c}
			Visual Studio & Code::Blocks \\
			\includegraphics[width=6cm]{images/breakpoint1.png} &
			\includegraphics[width=6cm]{images/breakpoint2.png}
		\end{tabular} ~\\
		
		You can also click on a line of code, and go to ~\\
		\textbf{Debug $>$ Toggle breakpoint} in Visual Studio and Code::Blocks.
	\end{hint}

	Run the program in debug mode: \textbf{Debug $>$ Start Debugging (F5)} in Visual Studio
	and \textbf{Debug $>$ Start/Continue (F8)} in Code::Blocks.
	~\\
	
	Choose ``1'' to start a new game. 
	Notice where your player is (the @ symbol), 
	and try to walk to an empty space (\# are walls, use W, A, S, or D and then ENTER to move.) 
	The program will pause after you hit ENTER and you can view the MovePlayer function. 
	It should be stopped at this line: \texttt{char firstLetter = direction[0];}

	\newpage
	In \textbf{Visual Studio}, you should see an \textbf{Autos} pane.
	If you don't see it, go to \textbf{Debug $>$ Windows $>$ Autos} to make it appear.

	\begin{center}
		\includegraphics[width=14cm]{images/lab11-autos.png}
	\end{center}
	
	And in \textbf{Code::Blocks},  you should see a \textbf{Watches} pane.
	If you don't see it, go to \textbf{Debug $>$ Debugging windows $>$ Watches} to make it appear.
	
	\begin{center}
		\includegraphics[width=14cm]{images/lab11-autos2.png}
	\end{center}
	
	\newpage
	Locate the following variables and write in their values in your text document...
	
	\paragraph{Log 1:}
	
	\begin{itemize}
		\item	direction
		\item	firstLetter
		\item	player
		\item	board
	\end{itemize}
	
	The player and board parameters are passed by reference. You may have a hexadecimal value, which is a memory address.
	~\\
	
	While the program execution is paused
	(thanks to our breakpoint), we can manually step through the program one line at a time
	to help us find errors and investigate variable values.
	Step to the next line of code by going to \textbf{Debug $>$ Step Over (F10)} in Visual Studio,
	or \textbf{Debug $>$ Next line (F7)} in Code::Blocks.
	The program execution should be stopped at this line: \texttt{firstLetter = tolower( firstLetter );}
	
	Now record the following value.
	
	\paragraph{Log 2:}
	
	\begin{itemize}
		\item	firstLetter
	\end{itemize}
	
	Next, look at the \texttt{MOVE\_UP} named constant in the if statement.
	If you right-click on that item, you can select \textbf{Go To Definition (F12)} (VS)
	or \textbf{Find declaration of} (C::B). It will take you up to where that item is
	declared in the program.
	
	\begin{center}
		\includegraphics[width=8cm]{images/consts.png}
	\end{center}
	
	Our breakpoint is still where we left it, however, so we can still continue
	stepping through from the same point. 
	
	\newpage
	Step to the next line of code (F10 in VS, F7 in C::B), 
	and your execution should now be paused at the first \textbf{if statement}.
	
	\begin{center}
		\includegraphics[width=8cm]{images/ifstatement.png}
	\end{center}
	
	At this point, we want to use \textbf{Debug $>$ Step Into (F11)} (VS) or
	\textbf{Debug $>$ Step Info (Shift+F7)} (C::B) to enter inside the if statement.
	It might not enter into the first if statement - it will enter whichever one corresponds
	to the direction you typed.~\\
	
	Now in the autos/watches window, expand the \texttt{player} item - it contains
	two variables within it: \texttt{x} and \texttt{y}.
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab11-playerxy.png} ~\\~\\
		\includegraphics[width=12cm]{images/lab11-playerxy2.png}
	\end{center}
	
	\paragraph{Log 3:}
	
	\begin{itemize}
		\item	player's x
		\item	player's y
	\end{itemize}
	
	\newpage
	Use \textbf{Step Into} again to move into the nested if statement
	
	\begin{center}
		\includegraphics[width=10cm]{images/nested.png}
	\end{center}
	
	Again, which if / else if statement it enters depends on what
	direction you typed in the program. If you told your character to
	walk into a wall, it won't get this far, so make sure you typed a direction
	with an empty space.
	~\\
	
	The player's \texttt{x} or \texttt{y} value won't change until you step
	to the line \textit{after} the increment/decrement statement, so watch
	your autos/watches window as you press \textbf{Step Into} again.
	The variable that changed should become highlighted, and you should
	get to the end of the function (since there was nothing else to step into.)
	
	\paragraph{Log 4:}
	
	\begin{itemize}
		\item	player's x
		\item	player's y
	\end{itemize}
	
	\subsection*{Task 2: YouWin function: Step into functions}
	
	Remove the original breakpoint at the beginning of the \textbf{MovePlayer}
	function, and set a new breakpoint at line 280, inside the \textbf{YouWin} function.
	You can also go to \textbf{Debug $>$ Continue} to have the program resume running normally again.
	
\begin{lstlisting}[style=code]
State YouWin()
{
    ClearScreen();
    cout << "YOU WIN!" << endl;
    // ...
\end{lstlisting}

	This function is called once the \texttt{@} character’s (x, y) 
	coordinates matches the (x, y) coordinates of the \texttt{\$} character,
	so to trigger this breakpoint, navigate your \texttt{@} to the \texttt{\$}.
	~\\
	
	Use \textbf{Step Over/Next Line} to step to the line where the \texttt{ClearScreen()}
	function is called and then use \textbf{Step Into}. It will take you into the
	ClearScreen function.
	
\begin{lstlisting}[style=code]
void ClearScreen()
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    system( "cls" );
    #else
    system( "clear" );
    #endif
}
\end{lstlisting}

	\paragraph{Log 5:} ~\\
	Which version of clear gets called - \texttt{system( "cls" );} or ~\\ \texttt{system( "clear" );}?

	~\\
	Click on \textbf{Step Over/Next Line} several times and once we reach the end of
	the ClearScreen function, we will return back to the YouWin function and continue
	tracing through it. Continue using \textbf{Step Over/Next Line} until you get to the 
	line of code: \texttt{string input = GetStringInput();} - use \textbf{Step Into} here.
		
\begin{lstlisting}[style=code]
string GetStringInput()
{
    cout << ">> ";
    string val;
    cin >> val;
    return val;
}
\end{lstlisting}
	
	\newpage
	Within the GetStringInput function, use \textbf{Step Over/Next Line} for each
	item. Once you step on the \texttt{cin >>} statement, control will go back to
	the game window because it's waiting for you to enter \texttt{'y'} or \texttt{'n'}. 

\begin{lstlisting}[style=output]
YOU WIN!
You found the treasure!

Go to the next level? (y/n): >> 
\end{lstlisting}

	\begin{hint}{Help! I `Stepped In' too deep!}
		If you use \textbf{Step Into} on certain lines of code, it will
		navigate into the standard library sometimes, and the code is
		difficult to read. Something like this: 
		
		~\\	
		\includegraphics[width=13cm]{images/lab11-weirdcode.png}
		
		If this happens to you, just use \textbf{Step Out} to leave this.
	\end{hint}
	
	Type in an answer into the console window (y or n) and hit enter, and
	the IDE will pause again at the next line of code - the \texttt{return val;}.
	Continuing to \textbf{Step Over/Next Line}, it will return once again to
	the YouWin function and decide to return either \texttt{MAINMENU} or \texttt{GAME}
	depending on your selection.

	\paragraph{Log 6:}
	
	\begin{itemize}
		\item	input
	\end{itemize}
	
	\newpage
	\subsection*{Task 3: Call stack}
	There is a debug pane that will tell you which functions has been 
	called to reach the point where you are at. ~\\
	
	Remove the breakpoint from within \textbf{YouWin} and set one at 
	the start of \textbf{GenerateMap} at line 178.
	
\begin{lstlisting}[style=code]
void GenerateMap( Board& board, Coords& player, Coords& treasure )
{
    // Fill in all the rooms
    for ( int y = 0; y < 10; y++ )
\end{lstlisting}

	Stop running the game and start in debug mode again
	and then select \textbf{Play} in the game's main menu.
	When the program pauses, look at the \textbf{Call Stack}.
	~\\
	
	If the Call Stack isn't visible, you can open it by going to 
	\textbf{Debug $>$ Windows $>$ Call Stack} in Visual Studio,
	or \textbf{Debug $>$ Debugging windows $>$ Call Stack} in Code::Blocks.
	
	
	\begin{center}
		\includegraphics[width=12cm]{images/lab11-callstack.png} ~\\~\\
		\includegraphics[width=12cm]{images/lab11-callstack2.png}
	\end{center}
	
	The top-most function listed is the function we're currently in.
	Each functino \textbf{below} it shows the path the program took
	to get here - \texttt{main()} at the bottom, then \texttt{Gameplay()},
	then \texttt{GenerateMap()}.
	
	Double-clicking on any of these lines will take you to within that function,
	where the next function was called.
	
	~\\
	Set a breakpoint at line 155, at the beginning of the \textbf{DrawMap} function
	and continue running the program with \textbf{Debug $>$ Continue}. When it stops
	at the DrawMap function, log the following:
	
	\paragraph{Log 7:}~\\
	
	Log each of the functions listed in the Call Stack.
	
	\hrulefill
	~\\~\\
	\begin{center}
		\includegraphics[width=5cm]{images/floppy-determined.png}
	\end{center}
	
	Being able to trace through code and inspect changing variables and the flow of the code
	is an important part of writing software. If you become a software developer,
	chances are you'll be working with a lot of code that \textit{other people wrote},
	rather than writing all new stuff. New features will mean hooking into existing features,
	fixing bugs means going through other peoples' code (or maybe your own code from months ago),
	and general maintenance means you'll be doing a lot of stepping through and investigation
	as you work on software.
	
	
\end{document}






