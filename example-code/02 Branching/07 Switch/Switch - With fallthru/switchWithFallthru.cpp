#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    cout << "ADOPT A PET" << endl;
    cout << "B. Bird adoption: $40" << endl;
    cout << "C. Cat  adoption: $50" << endl;
    cout << "D. Dog  adoption: $100" << endl;

    char choice;
    cout << "Adopt which: ";
    cin >> choice;

    float price = 0;

    switch ( choice )
    {
        case 'B':
        case 'b':
            price = 40;
        break;

        case 'C':
        case 'c':
            price = 50;
        break;

        case 'D':
        case 'd':
            price = 100;
        break;

        default:
            cout << "Invalid choice!" << endl;
    }

    cout << "Price: $" << price << endl;

    return 0;
}
