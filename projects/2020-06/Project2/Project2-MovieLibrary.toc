\contentsline {chapter}{\numberline {1}Project instructions}{2}
\contentsline {paragraph}{Topics:}{2}
\contentsline {chapter}{\numberline {2}Grading breakdown}{3}
\contentsline {chapter}{\numberline {3}Project 2: Movie Library}{5}
\contentsline {section}{\numberline {3.1}About: The program}{5}
\contentsline {section}{\numberline {3.2}About: Multiple files}{5}
\contentsline {section}{\numberline {3.3}Adding existing files in Visual Studio}{7}
\contentsline {section}{\numberline {3.4}Adding existing files in Code::Blocks}{8}
\contentsline {section}{\numberline {3.5}Overview: main()}{9}
\contentsline {paragraph}{Declarations:}{9}
\contentsline {paragraph}{MAX\_MOVIES:}{9}
\contentsline {paragraph}{savedMovies:}{9}
\contentsline {paragraph}{The array is full}{9}
\contentsline {paragraph}{Save and Load:}{10}
\contentsline {paragraph}{Inside the program loop:}{10}
\contentsline {paragraph}{All the function calls:}{10}
\contentsline {section}{\numberline {3.6}AddMovie function}{11}
\contentsline {paragraph}{Parameters:}{11}
\contentsline {paragraph}{Functionality:}{11}
\contentsline {paragraph}{Steps:}{11}
\contentsline {section}{\numberline {3.7}ViewAllMovies function}{13}
\contentsline {paragraph}{Parameters:}{13}
\contentsline {paragraph}{Functionality:}{13}
\contentsline {section}{\numberline {3.8}UpdateMovie function}{13}
\contentsline {paragraph}{Parameters:}{13}
\contentsline {paragraph}{Functionality:}{13}
\contentsline {section}{\numberline {3.9}ClearAllMovies function}{14}
\contentsline {paragraph}{Parameters:}{14}
\contentsline {paragraph}{Functionality:}{14}
\contentsline {section}{\numberline {3.10}SaveMovies function}{14}
\contentsline {paragraph}{Parameters:}{14}
\contentsline {paragraph}{Functionality:}{14}
\contentsline {section}{\numberline {3.11}LoadMovies function}{15}
\contentsline {paragraph}{Parameters:}{15}
\contentsline {paragraph}{Functionality:}{15}
\contentsline {section}{\numberline {3.12}Example output}{16}
\contentsline {paragraph}{Adding a movie:}{16}
\contentsline {paragraph}{Updating a movie:}{17}
\contentsline {paragraph}{View all movies:}{18}
\contentsline {paragraph}{Clear all movies:}{18}
