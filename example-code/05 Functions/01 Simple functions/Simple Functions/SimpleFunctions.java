public class SimpleFunctions
{
    public static void SayHello()
    {
        System.out.println( "Hello!" );
    }

    public static int GetNumber()
    {
        return 13;
    }

    public static double Add( double a, double b )
    {
        return a + b;
    }

    public static String FormatName( String first, String last )
    {
        return last + ", " + first;
    }
    
    public static void main( String args[] )
    {
        SayHello();

        System.out.println( "Favorite number is " + GetNumber() );

        double sum = Add( 2.5, 3.4 );
        System.out.println( "Sum is " + sum );

        String fname = "Harry";
        String lname = "Potter";
        String formatted = FormatName( fname, lname );
        System.out.println( formatted );
    }
}
