#include <iostream>
using namespace std;

#include "Player.hpp"

int main()
{
    Player players[2];
    players[0].Setup( "Player 0", 100 );
    players[1].Setup( "Player 1", 100 );

    Player* ptrCurrent = &players[0];
    Player* ptrEnemy = &players[1];

    int heavyAttack = 10;
    int defensiveAttack = 5;

    while ( players[0].IsAlive() && players[1].IsAlive() )
    {
        cout << endl << endl;
        for ( int i = 0; i < 2; i++ )
        {
            cout << players[i].GetHP() << " \t " << players[i].GetName() << endl;
        }

        cout << endl << ptrCurrent->GetName() << "'s turn" << endl << endl;

        cout << "1. Heavy attack" << endl;
        cout << "2. Defensive attack" << endl;
        int choice;
        cin >> choice;

        if ( choice == 1 )
        {
            ptrEnemy->TakeDamage( heavyAttack );
        }
        else if ( choice == 2 )
        {
            ptrEnemy->TakeDamage( defensiveAttack );
        }

        Player* temp = ptrCurrent;
        ptrCurrent = ptrEnemy;
        ptrEnemy = temp;
    }

    cout << "Game over" << endl;

    if ( players[0].IsAlive() )
    {
        cout << players[0].GetName() << " wins!" << endl;
    }
    else
    {
        cout << players[1].GetName() << " wins!" << endl;
    }

    return 0;
}
