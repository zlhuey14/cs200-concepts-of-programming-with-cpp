#include <iostream>
#include <string>
using namespace std;

string SayHi()
{
	return "Hello";
}

int main()
{
	int myInteger = 10;
	float myFloat = 19.99;

	int numberArray[] = { 1, 2, 3, 4, 5 };

	cout << "myInteger value: " << myInteger << "\t address: " << &myInteger << endl;
	cout << "myFloat value: " << myFloat << "\t address: " << &myFloat << endl;

	cout << endl;
	cout << "numberArray \t address: " << &numberArray << "\t" << numberArray << endl;

	for (int i = 0; i < 5; i++)
	{
		cout << "numberArray item #" << i << "'s value: " << numberArray[i] 
			<< "\t address: " << &numberArray[i] << endl;
	}

	cout << endl;
	cout << "Message: " << SayHi() << endl;
	cout << "Message: " << SayHi << endl;



	while (true);

	return 0;
}
