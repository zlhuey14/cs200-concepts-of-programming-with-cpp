#include "Players.hpp"


/* PLAYER CHARACTER ****************************************************/
/***********************************************************************/

PlayerCharacter::PlayerCharacter( string name )
    : Character( name ) {
    m_hp = 0; // placeholder
}

string PlayerCharacter::GetAction()
{
    return ""; // placeholder
}

int PlayerCharacter::GetDamageRoll() {
    return 0; // placeholder
}

/* NON-PLAYER CHARACTER ************************************************/
/***********************************************************************/

NonPlayerCharacter::NonPlayerCharacter( string name )
    : Character( name ) {
    m_hp = 0; // placeholder
}

string NonPlayerCharacter::GetAction()
{
    return ""; // placeholder
}

int NonPlayerCharacter::GetDamageRoll() {
    return 0; // placeholder
}

/* CHARACTER (parent class) FUNCTIONS **********************************/
/***********************************************************************/

Character::Character()
{
    m_healSlots = 2;
}

Character::Character( string name )
{
    m_healSlots = 2;
    SetName( name );
}

string Character::GetAction( int choice )
{
    if      ( choice == 1 ) { return "attack"; }
    else if ( choice == 2 ) { return "heal"; }
    else                    { return "unknown"; }
}

int Character::GetHP() {
    return m_hp;
}

int Character::GetArmor() {
    return m_armor;
}

string Character::GetName()
{
    return m_name;
}

void Character::SetName( string name )
{
    m_name = name;
}

int Character::GetHitRoll() {
    int die = rand() % 20 + 1;
    cout << m_name << " attacks with their " << m_weaponName << "!" << endl;
    return die + m_str;
}

int Character::GetDamageRoll() {
    return 0;
}

int Character::Heal() {
    if ( m_healSlots <= 0 )
    {
        cout << "Out of heal slots!" << endl;
        return 0;
    }

    int die = rand() % 8 + 1;
    m_healSlots--;
    ChangeHP( die );
    cout << m_name << " casts Cure Wounds and heals " << die << " hp!" << endl;
    return die;
}

void Character::TakeDamage( int hitRoll, int damageRoll ) {
    if ( hitRoll >= m_armor )
    {
        cout << m_name << " takes " << damageRoll << " damage!" << endl;
        ChangeHP( -damageRoll );
    }
    else
    {
        cout << "The attack misses..." << endl;
    }
}

void Character::ChangeHP( int amount )
{
    m_hp += amount;
    if ( m_hp <= 0 ) {
        m_hp = 0;
        cout << m_name << " falls unconscious!" << endl;
    }
    else if ( m_hp > m_maxHp ) { m_hp = m_maxHp; }
}
