#include <iostream>
#include <string>
using namespace std;

void Program1()
{
}

void Program2()
{
}

void Program3()
{
}

void Program4()
{
}

void Program5()
{
    float startingWage, percentRaisePerYear, adjustedWage;
    int yearsWorked, yearCounter;

    cout << "What is your starting wage?            ";
    cin >> startingWage;

    cout << "What % raise do you get per year?      ";
    cin >> percentRaisePerYear;

    cout << "How many years have you worked there?  ";
    cin >> yearsWorked;

    yearCounter = 0;
    adjustedWage = startingWage;

    cout << endl;

    while ( yearCounter < yearsWorked )
    {
        cout << "Salary at year " << (yearCounter+1) << ": " << adjustedWage << endl;
        adjustedWage = (adjustedWage * percentRaisePerYear / 100) + adjustedWage;
        yearCounter++;
    }
}

void Program6()
{
    int n;
    int i = 0;
    int sum = 0;

    cout << "Enter a value for n: ";
    cin >> n;

    while ( i <= n )
    {
        sum = sum + i;
        i++;
        cout << "Sum: " << sum << endl;
    }
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
