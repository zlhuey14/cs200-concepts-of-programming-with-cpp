public class WhileLoopCountdown
{
    public static void main( String args[] )
    {
        int counter = 100;

        while ( counter > 0 )
        {
            System.out.println( counter + "\t" );
            counter--;
        }

        System.out.println( "Done" );
    }
}
