\documentclass[a4paper,12pt,oneside]{book}
\usepackage[utf8]{inputenc}

\newcommand{\laTopic}       {While loops lab}
\newcommand{\laTitle}       {CS 200}
\renewcommand{\chaptername}{Part}
\renewcommand{\contentsname}{Contents - \laTopic}

\usepackage{../../rachwidgets}
\usepackage{../../rachdiagrams}

\title{}
\author{Rachel Singh}
\date{\today}

\pagestyle{fancy}
\fancyhf{}

\lhead{\laTopic \ / \laTitle}

\chead{}

\rhead{\thepage}

\rfoot{\tiny \thepage\ of \pageref{LastPage}}

\lfoot{\tiny Rachel Singh, last updated \today}

\renewcommand{\headrulewidth}{2pt}
\renewcommand{\footrulewidth}{1pt}

\begin{document}
	\tableofcontents

\chapter{Setup}
	\section{Turn-in instructions}

		\begin{itemize}
			\item	Once done, upload all source code files that you created/modified.
					This includes .cpp, .hpp, and/or .h files.
			\item	Don't zip the source files.
			\item	Don't zip the entire folder and upload that.
		\end{itemize}

	\section{Setup}

		\subsection{Using the starter code}

			Some \textbf{starter code} is provided for you for this lab.
			You can download the starter file off the Canvas assignment, or type in the starter code
			(next section).

		\paragraph{Project setup:} ~\\
		
			\subparagraph{Add existing file in Visual Studio:}

			\begin{enumerate}
				\item	Right-click on your project file in the \textbf{Solution Explorer}.
				\item	Select \textbf{Add} and choose \textbf{Existing item...}
				\item	Find the file and add it to the project.
			\end{enumerate}

			\subparagraph{Add existing file in Code::Blocks:}

			\begin{enumerate}
				\item	Right-click on your project file in the \textbf{Projects} pane.
				\item	Click \textbf{Add files...}
				\item	Find your file and click \textbf{Open}.
				\item	Make sure Debug and Release are both checked and click \textbf{OK}.
				\item	Make sure to save your project (File $>$ Save Project). 
			\end{enumerate}
			
			
		\subsection{Writing starter code from scratch}
			\paragraph{Project setup:} ~\\
			\begin{enumerate}
				\item	In your IDE, create a new project.
				
				\item	Add an existing file. ~\\
						\underline{In Visual Studio:} Right-click the project and select \textbf{Add... $\to$ New item...}
						and create your C++ Source File.
						~\\
						\underline{In Code::Blocks:} Go to \textbf{File $\to$ New... $\to$ Empty file}.
						It will ask ``Do you want to add this new file in the active project?'' - select Yes and
						give the file a name.
						
				\item	Locate your downloaded code file and select it to add it in.
			\end{enumerate}
		
\begin{lstlisting}[style=code]
#include <iostream>
#include <string>
using namespace std;

void Program1()
{
}

void Program2()
{
}

void Program3()
{
}

void Program4()
{
}

void Program5()
{
}

void Program6()
{
}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-6): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }
        else if ( choice == 5 ) { Program5(); }
        else if ( choice == 6 ) { Program6(); }

        cout << endl << "------------------" << endl;
    }

    return 0;
}
\end{lstlisting}

	~\\
	If you are downloading the file, make sure to create a new project/solution
	in Visual Studio/Code::Blocks first, and then move the \texttt{branchinglab.cpp}
	file into that folder.


\hrulefill
\subsection{About the starter code}

	This starter code uses \textbf{functions}, which we have not covered
	yet in this class, but they are handy for breaking up programs into
	smaller, more managable chunks. \textbf{You do not need to modify main()}
	for this lab. There are five mini-programs you will be implementing,
	and they will go under each \textbf{Program()} function, just like
	how you'd code in \texttt{main()} - except there's no \texttt{return 0;}.

	~\\
	When you run the program, it will ask you which program you want to run.
	You can test out each program this way.

\begin{lstlisting}[style=output]
Run which program? (1-6): 1


(Program 1 runs)

------------------------------------
Run which program? (1-6):
\end{lstlisting}










\chapter{While loops lab 1: Count Up}

	\paragraph{Program overview:} ~\\
		
\begin{lstlisting}[style=output]
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
\end{lstlisting}

	\begin{itemize}
		\item	This program will use a while loop and an integer variable to display a list
				of numbers from 0 to 20.
	\end{itemize}
	
		
	\hrulefill
	\section{Reference information}
	
		\subsection{How do I use a while loop?}
			A while loop is similar to an if statement, where it has some condition within its parentheses, but what is different is that the while loop will keep running its internal code while the condition is true, whereas an if statement only runs its internal code once. 

\begin{lstlisting}[style=code]
if ( CONDITION_IS_TRUE ) {
    // Do this once
}
    
while ( CONDITION_IS_TRUE ) {
    // Do this while the condition is still true
}
\end{lstlisting}

			A while loop will only stop looping once the condition evaluates to false, or if you use the break; command to escape the loop.
			
			~\\
			\textbf{Warning!} If there is no code INSIDE your while loop that will make its condition evaluate to false, then the loop will run forever. 
	
	\hrulefill
	\section{Specifications}
		Within the \texttt{Program1()} function, write a program that starts
		at 0 and counts up to 20, going up by 1 each time.
		You will need to declare and initialize an integer variable before your while loop,
		and make sure that the integer is incremented inside the while loop.
	
	
	\paragraph{Program flow:}
	
	\begin{enumerate}
		\item	Declare an integer (\texttt{int}) variable named something like \texttt{count}.
		\item	Assign the value 0 to \texttt{count} with an assignment statement. ~\\(\texttt{count = 0;})
		\item	Create a while loop: While \texttt{count} is \texttt{< 21}, or \texttt{<= 20}. Within the loop...
		\begin{enumerate}
			\item	Output the value of \texttt{count} with a \texttt{cout} statement.
			\item	Add 1 to \texttt{count} in one of the following ways: ~\\
					\texttt{count++;} ~\\
					\texttt{count += 1;} ~\\
					\texttt{count = count + 1;}
		\end{enumerate}
	\end{enumerate}

	\hrulefill
	\section{Testing}
	
		~\\ The program output should look like this:

\begin{lstlisting}[style=output]
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
\end{lstlisting}

%----------------------------------------------------------------------%










\chapter{While loops lab 2: Multiply up}

	\paragraph{Program overview:} ~\\

\begin{lstlisting}[style=output]
1 2 4 8 16 32 64 128
\end{lstlisting}

	\begin{itemize}
		\item	This program will be similar to program 1, but instead of going from 0 to 20, 
				it will go from 1 to 128, \textit{multiplying} \texttt{count} by 2 each time instead of
				\textit{adding} 1 each time.
	\end{itemize}
	
	\hrulefill
	\section{Reference information}
	
		\subsection{Math operations}
		
			\begin{tabular}{l | c | c | p{3cm}}
				\textbf{Operation} 	& \textbf{Long form} 		& \textbf{Short form} 	& \textbf{Special} \\ \hline
				Addition			& \texttt{num = num + 1}	& \texttt{num += 1}		& \texttt{num++}
				\\					&							&						& \texttt{++num}
				\\ \hline
				Subtraction			& \texttt{num = num - 1}	& \texttt{num -= 1}		& \texttt{num--}
				\\					&							&						& \texttt{--num}
				\\ \hline
				Multiplication		& \texttt{num = num * 2}	& \texttt{num *= 2}
				\\ \hline
				Division			& \texttt{num = num / 2}	& \texttt{num /= 2}
			\end{tabular}
			
			 ~\\~\\
			 \texttt{++} is the \textbf{increment operator}. It will increase a variable by 1,
			 and it can be used inside of another statement. For example, if you write:
		 
\begin{lstlisting}[style=code]
int a = 0;
cout << a++ << endl;	// outputs 0
cout << a << endl;		// outputs 1
\end{lstlisting}
		
			The variable \texttt{a} will have 1 added to it \textbf{after being output}.
			But, if you write:
		 
\begin{lstlisting}[style=code]
int a = 0;
cout << ++a << endl;	// outputs 1
cout << a << endl;		// outputs 1
\end{lstlisting}

			The variable \texttt{a} will have 1 added to it \textbf{prior to being output}.
			
			~\\
			\texttt{--} is the \textbf{decrement operator} and works in the same way,
			except it will decrease a variable by 1.

		
	\hrulefill
	\section{Specifications}
		
		\paragraph{Program flow:} ~\\
		
		\begin{enumerate}
			\item	Declare an integer (\texttt{int}) variable named something like \texttt{count}.
			\item	Assign the value 1 to \texttt{count} with an assignment statement. ~\\(\texttt{count = 0;})
			\item	Create a while loop: While \texttt{count} is less than or equal to 128. Within the loop...
			\begin{enumerate}
				\item	Output the value of \texttt{count} with a \texttt{cout} statement.
				\item	Double the value of \texttt{count} and store it back in that variable. You can do one of the following: ~\\
						\texttt{count *= 2;} ~\\
						\texttt{count = count * 2;}
			\end{enumerate}
		\end{enumerate}

	\hrulefill
	\section{Testing}
	
	~\\ The program output should look like this:

\begin{lstlisting}[style=output]
1 2 4 8 16 32 64 128
\end{lstlisting}

%----------------------------------------------------------------------%








\chapter{While loops lab 3: Number guesser}

	\paragraph{Program overview:} ~\\

		Write a program that will keep asking the user to guess a number, 
		telling the user whether they guessed too high, too low, or got the
		right number. Once the user has guessed the secret number, the program ends.
		
\begin{lstlisting}[style=output]
Guess a number:     5
Too low!

Guess a number:     10
Too high!

Guess a number:     8
That's right!

GAME OVER
\end{lstlisting}


	\hrulefill
	\section{Reference information}
	
		\subsection{Do... while loops}
		
	~\\ The form of a do...while loop is:
\begin{lstlisting}[style=code]
do
{
    // Do this stuff at least once
} while ( CONDITION_IS_TRUE );
\end{lstlisting}
		
	\hrulefill
	\section{Specifications}
		
		\paragraph{Program flow:} ~\\
		
			\begin{enumerate}
				\item	Declare variables \texttt{secretNumber} and \texttt{playerGuess}.
				\item	Initialize \texttt{secretNumber} to your favorite number.
				\item	do:
				\begin{enumerate}
					\item	Output the message, ``Guess a number:'' with a \texttt{cout} statement.
					\item	Get the user's input and store it in \texttt{playerGuess} using a \texttt{cin} statement.
					\item	If \texttt{playerGuess} is greater than \texttt{secretNumber}, output the message ``Too high!''
					\item	Otherwise if \texttt{playerGuess} is less than \texttt{secretNumber}, output the message ``Too low!''
					\item	Otherwise if \texttt{playerGuess} is equal to \texttt{secretNumber}, output the message ``That's right!''
				\end{enumerate}
				\item	while \texttt{playerGuess} is not \texttt{secretNumber}.
				\item 	Output the message ``GAME OVER''
			\end{enumerate}

	\hrulefill
	\section{Testing}
	~\\ The program output will look like:

\begin{lstlisting}[style=output]
Guess a number:     5
Too low!

Guess a number:     10
Too high!

Guess a number:     8
That's right!

GAME OVER
\end{lstlisting}

%----------------------------------------------------------------------%






\chapter{While loops lab 4: Input validator}

	\paragraph{Program overview:} ~\\

	In this program, we will write a simple input validator, which will be
	useful in future programs as well. Here, we will display a message to the
	user asking them to enter a number between 1 and 5.
	If they enter something \textit{outside} of that range, then
	an error message will be displayed and they will be required to 
	enter another number - this continues until they finally choose a
	value between 1 and 5.
	
\begin{lstlisting}[style=output]
Please enter a number between 1 and 5: 100
Invalid entry, try again: 30
Invalid entry, try again: -2
Invalid entry, try again: 3
Thank you.
\end{lstlisting}

	\begin{itemize}
		\item	Valid input:	Between 1 and 5, inclusive.
		\item	Invalid input:	Less than 1 or greater than 5, exclusive.
	\end{itemize}
		
	\newpage
	\section{Reference information}
	
		\subsection{Is the number within a valid range?}
		
			We went over this in the branching lab as well, but you can 
			check if the input is valid or you can check if it's invalid.
			
			\paragraph{Valid entry:} ~\\
			
			\begin{center}
				\includegraphics[width=6cm]{images/range1.png}
			\end{center}
			
			The number is valid if it is $\geq$ the minimum, AND it is $\leq$ the maximum.
			
			\paragraph{Invalid entry:} ~\\
			
			\begin{center}
				\includegraphics[width=6cm]{images/range2.png}
			\end{center}
			
			The number is invalid if it is $<$ the minimum, OR it is $>$ the maximum.
		
	\hrulefill
	\section{Specifications}
		
		\paragraph{Program flow:} ~\\
		
		\begin{enumerate}
			\item	Create a variable to store the user's input, it should be an integer type.
			\item	Ask the user to enter a number between 1 and 5.
			\item	Get the user's input and store it in your variable.
			\item	Using a while loop, continue looping while their input is invalid...
			\begin{enumerate}
				\item	Display a message, ``Invalid entry, try again:''
				\item	Get the user's input and store it in your variable.
			\end{enumerate}
			\item	After the while loop, the input is valid. Display a ``Thank you'' message.
		\end{enumerate}

	\hrulefill
	\section{Testing}

		~\\ The output should look like:
\begin{lstlisting}[style=output]
Please enter a number between 1 and 5: 100
Invalid entry, try again: 30
Invalid entry, try again: -2
Invalid entry, try again: 3
Thank you.
\end{lstlisting}

%----------------------------------------------------------------------%




\newpage
\subsection*{Program 5: Getting a raise}

	\paragraph{Program overview:} ~\\
		
		This program will calculate your salary after $n$ amount of years,
		getting some $x$\% raise each year.
	
\begin{lstlisting}[style=output]
What is your starting wage?             100000
What % raise do you get per year?       5
How many years have you worked there?   8

Salary at year 1:   100000
Salary at year 2:   105000
Salary at year 3:   110259
Salary at year 4:   115762
Salary at year 5:   121551
Salary at year 6:   127628
Salary at year 7:   134010
Salary at year 8:   140710
\end{lstlisting}
		
	\hrulefill
	\section{Specifications}
		
		\paragraph{Variables:} ~\\
		
		\begin{center}	
			\begin{tabular}{l l p{6cm}}
				\textbf{Variable name} 			& \textbf{Data type}	& \textbf{Description} \\ \hline
				\texttt{startingWage}			& \texttt{float}		& The user's starting wage \\
				\texttt{percentRaisePerYear}	& \texttt{float}		& The \% wage increase they get per year \\
				\texttt{adjustedWage}			& \texttt{float}		& The adjusted wage, calculated each year \\
				\texttt{yearsWorked}			& \texttt{int}			& The amount of years the user will work \\
				\texttt{yearCounter}			& \texttt{int}			& The year counter used to calculate for each year \\
			\end{tabular}
		\end{center}
		
		\paragraph{Program information:} ~\\
		
			Ask the user to enter data for each of these items, except the \texttt{yearsCounter}, 
			which will be used later, and \texttt{adjustedWage}. Initialize \texttt{yearsCounter} to 0.

			After the user has entered the \texttt{startingWage},
			set \texttt{adjustedWage}'s initial value to \texttt{startingWage}.

			You'll create a while loop that will iterate \textit{yearsWorked} amount of years, 
			recalculating the new salary for each year. The equation to calculate the latest wage is:

			\begin{center} \footnotesize
			\texttt{adjustedWage = (adjustedWage * percentRaisePerYear / 100) + adjustedWage.}
			\end{center}

			Make sure to display the wage value each time you calculate it in the loop.

		\begin{hint}{How do I run the loop the right amount of times?}
			
			You'll create a while loop that will continue while the \texttt{yearCounter} is less than the \texttt{yearsWorked}, 
			and make sure to add 1 to the \texttt{yearCounter} each time. 
			
		\end{hint}

	\hrulefill
	\section{Testing}
	
		~\\ The output should look like this:
\begin{lstlisting}[style=output]
What is your starting wage?             100000
What % raise do you get per year?       5
How many years have you worked there?   8

Salary at year 1:   100000
Salary at year 2:   105000
Salary at year 3:   110259
Salary at year 4:   115762
Salary at year 5:   121551
Salary at year 6:   127628
Salary at year 7:   134010
Salary at year 8:   140710
\end{lstlisting}

		\begin{hint}{How do I make these \$ amounts look nicer?}
		There isn't a built-in, easy way to display currency values like ``\$105,000'' in one simple step.
		... So, sorry, don't worry about it for this class. :)
		\end{hint}
	
	
%----------------------------------------------------------------------%

	
	
	
	


\newpage
\chapter{While loop lab 6: Summation}

	\paragraph{Program overview:} ~\\
	
		For this program, you will add the sum of all the numbers from 1 to $n$ (the user enters $n$) using a loop.

\begin{lstlisting}[style=output]
Enter a value for n: 5

Sum: 1
Sum: 3
Sum: 6
Sum: 10
Sum: 15
\end{lstlisting}
		
	\hrulefill
	\section{Specifications}
			
		\paragraph{Variables:} 
		
		\begin{center}	
			\begin{tabular}{l l l p{6cm}}
				\textbf{Variable} 				& \textbf{Data type}	& \textbf{Initial value} 	& \textbf{Description} \\ \hline
				\texttt{n}						& \texttt{int}			& 	& The ending values $n$ to add up until \\
				\texttt{counter}				& \texttt{int}			& 1	& A number from 1 to $n$ \\
				\texttt{sum}					& \texttt{int}			& 0	& The sum of numbers from 1 to $n$ \\
			\end{tabular}
		\end{center}
		
		\newpage
		\paragraph{Program information:} ~\\
		
			Initialize the \texttt{counter} to 1 and the \texttt{sum} to 0 then ask the user a value for $n$.~\\
			
			Create a loop that loops while \texttt{counter <= n}. Within the loop,
			add the value of \texttt{counter} onto the sum (\texttt{sum += counter}), and increment \texttt{counter} (\texttt{counter++}),
			and display the value of \texttt{sum} each time (using a \texttt{cout}).

	\hrulefill
	\section{Testing}
	
		~\\ The output should look like this:
\begin{lstlisting}[style=output]
Enter a value for n: 5

Sum: 1
Sum: 3
Sum: 6
Sum: 10
Sum: 15
\end{lstlisting}

	\begin{hint}{One fewer variable}
		You could also implement this without the \texttt{counter} variable:
		You could loop while $n$ is greater than 0, adding $n$ onto the sum each time,
		and decrementing $n$ each cycle.
		
		$$ 5 + 4 + 3 + 2 + 1 \equiv 1 + 2 + 3 + 4 + 5 $$
	\end{hint}
	

%----------------------------------------------------------------------%



\end{document}


