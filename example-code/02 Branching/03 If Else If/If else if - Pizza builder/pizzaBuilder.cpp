#include <iostream>     // cin and cout
using namespace std;    // removes std:: prefix (std::cin, std::cout)

int main()
{
    cout << "PIZZA BUILDER" << endl;

    cout << "1. Cheese Pizza" << endl;
    cout << "2. Pepperoni Pizza" << endl;
    cout << "3. Pineapple + Canadian Bacon Pizza" << endl;
    cout << "4. Buffalo Chicken Pizza" << endl;
    cout << "5. Lotsa Veggies Pizza" << endl;
    cout << "6. Vegan Veggies Pizza" << endl;

    int pizzaChoice;
    cout << "Which pizza: ";
    cin >> pizzaChoice;

    float price;

    const float BASE_PIZZA_PRICE = 9.00;
    const float TOPPING_PRICE = 0.40;
    const float CHEESE_PRICE = 0.30;
    const float SAUCE_PRICE = 0.25;

    if ( pizzaChoice == 1 )
    {
        price = BASE_PIZZA_PRICE + CHEESE_PRICE;
    }
    else if ( pizzaChoice == 2 )
    {
        price = BASE_PIZZA_PRICE + CHEESE_PRICE + TOPPING_PRICE;
    }
    else if ( pizzaChoice == 3 )
    {
        price = BASE_PIZZA_PRICE + CHEESE_PRICE + (2 * TOPPING_PRICE);
    }
    else if ( pizzaChoice == 4 )
    {
        price = BASE_PIZZA_PRICE + CHEESE_PRICE + TOPPING_PRICE + SAUCE_PRICE;
    }
    else if ( pizzaChoice == 5 )
    {
        price = BASE_PIZZA_PRICE + CHEESE_PRICE + (5 * TOPPING_PRICE);
    }
    else if ( pizzaChoice == 6 )
    {
        price = BASE_PIZZA_PRICE + (5 * TOPPING_PRICE);
    }
    else
    {
        cout << "Invalid pizza selection!" << endl;
    }

    cout << endl << "Cost of pizza: $" << price << endl;

    return 0;
}

